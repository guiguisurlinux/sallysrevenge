using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paparmane : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //Plays that collision sound
    void OnTriggerEnter2D (Collider2D other)
    {
        if (  other.name == "Sally"  )
        {
            print("Paparmane trigger");
            GameObject.Find("FlashyPanel").GetComponent<FlashyPanel>().Flash("Paparmane! Miam!");
            GameObject.Find("SFXManager/Paparmane").GetComponent<AudioSource> ().Play ();
            Destroy(gameObject);
        }
    }
}
