using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
    public Sally sally;
    public int upgradeType = 0;
    public string message = "";

    // Start is called before the first frame update
    void Start()
    {
        // Check if the player already got this upgrade
        if (  GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl2Checkpoint  )
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //Plays that upgrade sound
    void OnTriggerEnter2D (Collider2D other)
    {
        if (  other.name == "Sally"  )
        {
            if (  upgradeType == 0  )
            {
                sally.GetLaser();
                GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl1Checkpoint = true;
            }
            
            if (  upgradeType == 1  )
            {
                GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl2Checkpoint = true;
            }
            
           if (  message != ""  )
           {
               GameObject.Find("FlashyPanel").GetComponent<FlashyPanel>().Flash(message);
           }
            
            GameObject.Find("SFXManager/Upgrade").GetComponent<AudioSource> ().Play ();
            GetComponent<Renderer>().enabled = false;
            Destroy(gameObject);
        }
    }
}
