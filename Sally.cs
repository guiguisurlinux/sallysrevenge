using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sally : MonoBehaviour
{

    public bool invicible = false;
    public bool infiniteEnergy = false;

    [SerializeField]
    Transform DeathPanel; //Will assign our panel to this variable so we can enable/disable it

    public bool gotLaser = false;
    public bool inRecovery = false;
    public float speed = 0.1f;
    public float breakingFactor = 0.5f;
    public float accelerationFactor = 0.5f;
    private float xMovement = 0f;
    private float yMovement = 0f;
    public float maxEnergy = 1160;
    public float currentEnergy = 1160;
    public float energySpendRate = 100;
    public float energyRefillRate = 25;
    public float energyRecoveryRefillRate = 10;
    public float energyCriticalThreshold = 20;
    public float laserEnergyCost = 500;
    public EnergyBar energyBar;

    public Vector2 move;
    
    public GameObject laserPrefab;
    public Transform firepoint;

    // Start is called before the first frame update
    void Start()
    {
        DeathPanel.gameObject.SetActive(false); //make sure our pause menu is disabled when scene starts

        energyBar.SetMaxEnergy( currentEnergy / 10 );
        energyBar.SetEnergy( currentEnergy / 10 );
        
        if (  SceneManager.GetActiveScene().name == "level1" && GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl1Checkpoint  )
        {
            print("Skipping to checkpoint (level 1)");
            GotoCheckpoint1();
        }
        else if (  SceneManager.GetActiveScene().name == "level2" && GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl2Checkpoint  )
        {
            print("Skipping to checkpoint (level 2)");
            GotoCheckpoint1();
        }
    }

    // Update is called once per frame
    void Update()
    {
        _processPlayerMovement ();
        _processEnergyRefillUpdate ();
        
        //_printStuff();
    }
    
    private void _printStuff()
    {
        print("Current energy: " + currentEnergy);
        print("Spend rate: " + energySpendRate);
        print("Move x: " + move.x * Time.deltaTime);
        print("Move y: " + move.y * Time.deltaTime);
    }

    private void _processEnergyRefillUpdate ()
    {
        if (  infiniteEnergy  )
        {
            currentEnergy = maxEnergy;
            return;
        }

        // Only refill when not spending fuel, otherwise the interaction complicates calibration
        if (  inRecovery  ) {
            currentEnergy += energyRecoveryRefillRate * Time.deltaTime;
        }
        else if (  move.x == 0  )
        {
            currentEnergy += energyRefillRate * Time.deltaTime;
        }
        
        if (  currentEnergy >= maxEnergy  )
        {
            if (  inRecovery  )
            {
                energyBar.stopRecovery();
            }
        
            currentEnergy = maxEnergy;
            inRecovery = false;
        }
        if (  currentEnergy < 0  )
        {
            currentEnergy = 0;
        }

        if (  currentEnergy <= energyCriticalThreshold  )
        {
            if (  !inRecovery  )
            {
                energyBar.startRecovery();
            }

            inRecovery = true;
        }
        
        energyBar.SetEnergy( (int)(currentEnergy / 10) );
    }
    
    private void _processPlayerMovement ()
    {   
        yMovement = 0f;
        xMovement = speed * Time.deltaTime;

        // The camera always advances
        Camera.main.transform.Translate(speed * Time.deltaTime, 0f, 0f);

        if (  move.y > 0  )
        {
            yMovement += speed * Time.deltaTime;
        }

        if (  move.y < 0  )
        {
            yMovement -= speed * Time.deltaTime;
        }

        if (  !inRecovery  )
        {
            if (  move.x < 0  )
            {
                if (currentEnergy > 0)
                {
                    currentEnergy -= energySpendRate * Time.deltaTime;
                    xMovement -= speed * Time.deltaTime * breakingFactor;
                }
            }
                
            if (  move.x > 0  )
            {
                if (currentEnergy > 0)
                {
                    currentEnergy -= energySpendRate * Time.deltaTime;
                    xMovement += speed * Time.deltaTime * accelerationFactor;
                }
            }
        }

        // Move this (the player)        
        transform.Translate(xMovement, yMovement, 0f);
        
        // Move the camera up or down with the player
        Camera.main.transform.Translate(0f, yMovement, 0f);
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!invicible)
        {
            GameObject.Find("SFXManager/Explosion").GetComponent<AudioSource> ().Play ();

            DeathPanel.gameObject.SetActive(true);
            Destroy(gameObject);
        }
    }
    
    public void GetLaser()
    {
        gotLaser = true;
    }
    
    public void Shoot()
    {
        if (  gotLaser && !inRecovery )
        {
            Instantiate(laserPrefab, firepoint.position, firepoint.rotation);
            currentEnergy -= laserEnergyCost;
            GameObject.Find("SFXManager/Pewpew").GetComponent<AudioSource> ().Play ();
        }
    }

    public void GotoCheckpoint1()
    {
        transform.position = GameObject.Find("Checkpoint1").transform.position;
        Camera.main.transform.position = GameObject.Find("Checkpoint1").transform.position;
        Camera.main.transform.Translate(5f, 0f, -10f);
    }
}
