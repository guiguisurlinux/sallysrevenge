﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{

    public Slider slider;
    public Image fill;
    public Text energyHUD;
    
    // Start is called before the first frame update
    void Start()
    {

    }
    
    public void startRecovery()
    {
        GameObject.Find("SFXManager/LowEnergy").GetComponent<AudioSource> ().Play ();
    }
    
    public void stopRecovery()
    {
        GameObject.Find("SFXManager/LowEnergy").GetComponent<AudioSource> ().Stop ();
    }

    public void SetMaxEnergy(float energy)
    {
		slider.maxValue = energy;
		slider.value = energy;
    }
    
    public void SetEnergy(float energy)
    {
		slider.value = energy;
		energyHUD.text = energy.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
