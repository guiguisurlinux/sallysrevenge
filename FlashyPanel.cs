using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashyPanel : MonoBehaviour
{
    Transform textUI;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Flash(string message, int times = 4)
    {
        Transform child = transform.GetChild(0);
        child.GetComponent<Text>().text = message;
    
        child.gameObject.SetActive(true);
        
        StartCoroutine(FlashyFlash(child.gameObject, times));
    }
    
    IEnumerator FlashyFlash(GameObject element, int times)
    {
        for (int i = 0; i < times * 2; i++)
        {
            yield return new WaitForSeconds(0.3f);
            element.SetActive(!element.activeSelf);
        }
        
        element.SetActive(false);
    }
}
