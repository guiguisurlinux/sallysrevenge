using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{

    public int phase = 0;
    bool triggered = false;
    GameObject mainCamera;
    public GameObject[] cores;
    

    // Start is called before the first frame update
    void Start()
    {
            mainCamera = GameObject.Find("MainCamera");
    }
    
    // Update is called once per frame
    void Update()
    {
        if (  !triggered && transform.position.x <= (mainCamera.transform.position.x + 16) )
        {
            triggered = true;
            cores[phase].SetActive(true);
        }
    }
    
    public void StartNextPhase()
    {
        phase++;
        
        if (phase < cores.Length) {
            cores[phase].SetActive(true);
        }
        else
        {
            StartCoroutine(DieSlower());
        }
    }
    
    IEnumerator DieSlower()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("endscreen");
    }
}
