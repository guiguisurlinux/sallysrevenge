using UnityEngine;
using System.Collections;
using UnityEngine.UI; //Need this for calling UI scripts
using UnityEngine.SceneManagement;
using System;

public class Manager : MonoBehaviour {

    [SerializeField]
    Transform UIPanel; //Will assign our panel to this variable so we can enable/disable it

    [SerializeField]
    FlashyPanel flashyPanel;

    bool isPaused; //Used to determine paused state
    
    
    public string startMessage = "";
 
    public Transform player;
    public Transform mainCamera;
    public Transform playerSpawnPoint;
    public Transform cameraStartPoint;
    public Transform checkPoint1;
    public Sally sally;
    public InputMaster controls;
    
    Vector2 move;
    
    void Awake()
    {
        controls = new InputMaster();
        
        controls.Player.Pause.performed += ctx => TogglePause();
        controls.Player.Movement.performed += ctx => move = ctx.ReadValue<Vector2>();
        controls.Player.Movement.canceled += ctx => move = Vector2.zero;
        controls.Player.Shoot.performed += ctx => sally.Shoot();
//        controls.Player.Cheat1.performed += ctx => sally.GotoCheckpoint1();
    }
    
    void OnEnable()
    {
        controls.Player.Enable();
    }
    
    void OnDisable()
    {
        controls.Player.Disable();
    }

    void Start ()
    {
        UIPanel.gameObject.SetActive(false); //make sure our pause menu is disabled when scene starts
        isPaused = false; //make sure isPaused is always false when our scene opens
        
        // Initialize player location
        player.position = playerSpawnPoint.position;
        
        // Initialize camera location
        mainCamera.position = cameraStartPoint.position;
        
       if (  startMessage != ""  )
       {
        flashyPanel.Flash(startMessage);
       }
    }

    void Update ()
    {    
        // If unpaused be give the player the move instructions, if paused shut down everything    
        if (  !isPaused  )
        {
            sally.move = move;
        }
        else
        {
            sally.move = Vector2.zero;
        }
    }

    public void TogglePause()
    {
        if (  isPaused  )
        {
            UnPause();
        }
        else
        {
            Pause();
        }
    }

    public void Pause()
    {
        isPaused = true;
        UIPanel.gameObject.SetActive(true); //turn on the pause menu
        Time.timeScale = 0f; //pause the game
    }

    public void UnPause()
    {
        isPaused = false;
        UIPanel.gameObject.SetActive(false); //turn off pause menu
        Time.timeScale = 1f; //resume game
    }

    public void QuitGame()
    {
         #if UNITY_EDITOR
             UnityEditor.EditorApplication.isPlaying = false;
         #else
             Application.Quit();
         #endif
    }

    public void RestartFromCheckpoint()
    {
        UnPause();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    

    public void RestartLevel()
    {
        // Just pretend we didn't get the checkpoints
        GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl1Checkpoint = false;
        GameObject.Find("SceneVariables").GetComponent<SceneVariables>().lvl2Checkpoint = false;
        RestartFromCheckpoint();
    }
}
