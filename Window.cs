using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    public float speed = 2f;
    public float rotationSpeed = 6f;
    bool triggered = false;
    Vector3 currentEulerAngles;
    Transform child;
    GameObject mainCamera;

    // Start is called before the first frame update
    void Start()
    {
            // Start with random rotation
            child = gameObject.transform.GetChild(0);
            child.RotateAround(child.position, Vector3.forward, Random.Range(-90.0f, 90.0f));
            
            mainCamera = GameObject.Find("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (  !triggered && transform.position.x <= (mainCamera.transform.position.x + 16) )
        {
            triggered = true;
        }

        if (  triggered  )
        {
            // Move this and rotate the child within
            transform.Translate(-speed * Time.deltaTime, 0f, 0f);

            child = gameObject.transform.GetChild(0);
            child.RotateAround(child.position, Vector3.forward, rotationSpeed * Time.deltaTime);
        }
    }

}
