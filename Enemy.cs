using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health = 1f;
    public bool invincible = false;
    public string dieSound = "";
    public string hitSound = "";
    
    void OnBecameInvisible() {

    }

    public void Hit(float damage = 1f)
    {   
        if (  hitSound != ""  &&  transform.GetComponent<Renderer>().isVisible  )
        {
            GameObject.Find("SFXManager/" + hitSound).GetComponent<AudioSource> ().Play ();
        }

        if (!invincible)
        {
            GetDamaged(damage);
            
            if (  health <= 0  ) {
                Die();
            }
        }
    }
    
    public virtual void GetDamaged(float damage = 1f)
    {

        if (  transform.parent != null  )
        {
            transform.parent.GetComponent<EnemyCluster>().childHit();
        }

        health -= damage;
    }
    
    public virtual void Die()
    {
        if (  dieSound != ""  &&  transform.GetComponent<Renderer>().isVisible  )
        {
            GameObject.Find("SFXManager/" + dieSound).GetComponent<AudioSource> ().Play ();
        }

        if (  transform.parent != null  )
        {
            transform.parent.GetComponent<EnemyCluster>().childDestroyed();
        }
    
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // todo: Variable laser strength
        Hit(1f);
    }
}
