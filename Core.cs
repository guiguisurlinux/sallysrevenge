using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : Enemy
{
    public GameObject[] defensiveCircles;
    
    public float speed = 2f;
    public float rotationSpeed = 0f;
    public float acceleration = 0f;
    public float maxSpeed = 20f;
    public float minSpeed = -20f;
    public float upperBound = 0f;
    public float lowerBound = 0f;
    public bool selfDestructer = false;
    public float timeBeforeSelfDestruct = 5f;
    public bool flamerCore = false;
    public bool finalCore = false;

    GameObject boss;
    GameObject mainCamera;
    float relativeX;
    float rotationAdded;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("MainCamera");
    }
    
    // Awaken!
    void Awake()
    {
        foreach (GameObject defensiveCircle in defensiveCircles)
        {
            // Rotate child around self
            defensiveCircle.SetActive(true);
        }
        
        boss = transform.parent.gameObject;
        
        if (  selfDestructer  )
        {
            StartCoroutine(InitiateSelfDestruct());
        }
        
        if (  flamerCore  )
        {
            StartCoroutine(ShedFlamers());
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        int rotationDirection = 1;
        DoMovementAndAcceleration();

        // Move this and rotate the children within
        rotationAdded = rotationSpeed * Time.deltaTime;

        foreach (GameObject defensiveCircle in defensiveCircles)
        {
            // Rotate child around the boss' middle point.
            defensiveCircle.transform.RotateAround(boss.transform.position, Vector3.forward, rotationAdded * rotationDirection);

            // Invert rotation so adjacent circles move in opposite directions.
            // Not sure if the order of iteration in the foreach is guaranteed but seems to work.
            rotationDirection = rotationDirection * -1;
        }
    }
    
    void DoMovementAndAcceleration()
    {
        // The camera is the "anchor point", so the boss moves relative to the screen and not the stage.
        relativeX = boss.transform.position.x - mainCamera.transform.position.x;

        //print("Current speed: " + speed);

        boss.transform.Translate(speed * Time.deltaTime, 0f, 0f);

        // This allows rubberbanding without fiddling with speed VS acceleration too much.
        if (  relativeX > upperBound && speed > minSpeed  )
        {
            speed -= Time.deltaTime * acceleration;
        }
        
        if (  relativeX < lowerBound && speed < maxSpeed  )
        {
            speed += Time.deltaTime * acceleration;
        }
    }
    
    IEnumerator InitiateSelfDestruct()
    {
        yield return new WaitForSeconds(5);
        
        //After we have waited 5 seconds print the time again.
        Die();
    }
    
    IEnumerator ShedFlamers()
    {
        // This ejects each flamer in the group in turn, with 3 seconds in-between
        for (int i = 0; i < 15; i++)
        {
            defensiveCircles[0].transform.GetChild(0).GetComponent<Flamer>().speedY = i * 2.5f;
            yield return new WaitForSeconds(3);
        }
    }

    // Override normal damage, this enemy has no parent :'(
    public override void GetDamaged(float damage = 1f)
    {
        if (  !invincible  )
        {
            health -= damage;
        }
    }

    // Override normal death, this is a special kind of enemy not part of a cluster
    public override void Die()
    {    
        foreach (GameObject defensiveCircle in defensiveCircles)
        {
            // Rotate child around self
            defensiveCircle.SetActive(false);
        }

        GetComponent<BoxCollider2D> ().enabled = false;

        print("Dead: " + name);

        boss.GetComponent<Boss>().StartNextPhase();
        
        Destroy(gameObject);
    }
}
