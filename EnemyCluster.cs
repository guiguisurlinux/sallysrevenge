using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCluster : MonoBehaviour
{    
    public int numberOfChildren = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void childHit(float damage = 1f)
    {
        // Could do something
    }
    
    public void childDestroyed()
    {
        // The container doesn't need to exist if it has no children
        numberOfChildren -= 1;
        
        if (  numberOfChildren <= 0  )
        {
            Destroy(gameObject);
        }
    }
}
