using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public Transform beeper;
    public float speed = 40f;
    public float triggerDistance = 20f;
    bool triggered = false;
    GameObject mainCamera;
    Transform newBeeper;
    Transform HUDPanel;

    // Start is called before the first frame update
    void Start()
    {            
            mainCamera = GameObject.Find("MainCamera");
            HUDPanel = GameObject.Find("HUDPanel").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (  !triggered && transform.position.x <= (mainCamera.transform.position.x + triggerDistance) )
        {
            triggered = true;
            newBeeper = Instantiate(beeper, HUDPanel);
            newBeeper.gameObject.SetActive(true);
            print("New beeper activated");
        }

        if (  triggered  )
        {
            Vector3 screenPos = mainCamera.GetComponent<Camera>().WorldToScreenPoint(transform.position);
            Vector3 newPos = newBeeper.localPosition;

            newPos.y = screenPos.y - 250;
            newBeeper.localPosition = newPos;
        }

        // Trigger the 
        if (  triggered  &&  (GetComponent<Renderer>().isVisible || transform.position.x <= (mainCamera.transform.position.x + 5) ) )
        {
            transform.Translate(-speed * Time.deltaTime, 0f, 0f);
            
            if (  newBeeper.gameObject.activeSelf  )
            {
                newBeeper.gameObject.SetActive(false);
            }
        }
    }
}
