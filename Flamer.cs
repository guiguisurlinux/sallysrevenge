using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamer : MonoBehaviour
{
    public float initialShootingDelay = 0f;
    public float warningTime = 1f;
    public float shootingTime = 2f;
    public float rechargeTime = 1f;
    public float warningAlpha = 0.2f;
    public float shootingAlpha = 1f;
    public float speedX = 2f;
    public float speedY = 0f;
    
    
    bool initialized = false;
    Renderer laserRenderer;
    CapsuleCollider2D laserBox;
    GameObject laser;
    GameObject mainCamera;
    Renderer mainRenderer;
    Color laserColor;
    SpriteRenderer laserSpriteRenderer;

    void Awake()
    {
        laser = transform.Find("Beam").gameObject;
        mainRenderer = transform.Find("Red").gameObject.GetComponent<Renderer>();
        laserRenderer = laser.GetComponent<Renderer>();
        laserBox = laser.GetComponent<CapsuleCollider2D>();
        laserSpriteRenderer = laser.GetComponent<SpriteRenderer>();
        laserColor = laser.GetComponent<SpriteRenderer>().color;

        laser.SetActive(false);
        laserBox.enabled = false;
        mainCamera = GameObject.Find("MainCamera");
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (  !initialized && transform.position.x <= (mainCamera.transform.position.x + 16) )
        {
            initialized = true;
            StartCoroutine("BlinkLaser");
        }
        
        if (  initialized  )
        {
            // Move this
            transform.Translate(-speedX * Time.deltaTime, -speedY * Time.deltaTime, 0f);
        }
    }
    
    private IEnumerator BlinkLaser()
    {
        yield return new WaitForSeconds (initialShootingDelay);
        while (true)
        {
            // Warning phase
            laserColor.a = warningAlpha;
            laserSpriteRenderer.color = laserColor;
            laser.SetActive(true);
            laserBox.enabled = false;
            yield return new WaitForSeconds (warningTime);
            // Active phase
            laserColor.a = shootingAlpha;
            laserSpriteRenderer.color = laserColor;
            laser.SetActive(true);
            laserBox.enabled = true;
            yield return new WaitForSeconds (shootingTime);
            // Recharge phase
            laserColor.a = 0f;
            laserSpriteRenderer.color = laserColor;
            laser.SetActive(false);
            laserBox.enabled = false;
            yield return new WaitForSeconds (rechargeTime);
        }
    }
}
