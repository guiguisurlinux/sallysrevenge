using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    GameObject hit;
    Enemy damagedEnemy;
    
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }
    
    void OnBecameInvisible() {
        enabled = false;
        
        Destroy(gameObject);
    }

    //Plays that collision sound
    void OnTriggerEnter2D (Collider2D hitInfo)
    {

        if (  hitInfo.name.Length == 5 && hitInfo.name.Substring(0,5) == "Laser"  ) {
            print("Laser hit laser");
        }
        else
        {
            // We hit something
            hit = hitInfo.gameObject;
            
            damagedEnemy = hit.transform.GetComponent<Enemy>();

            if (  damagedEnemy != null  )
            {
                damagedEnemy.Hit();
            }
        }

        Destroy(gameObject);
    }

}
